"""Command-line client for Hrvatski Jezični Portal."""
import os
import sys

from dataclasses import dataclass
from functools import reduce
from typing import List, Optional
from urllib.parse import urljoin

import argparse
from lxml import html
import requests

# FIXME: Remove once verification of HJP's certificate works again.
import urllib3
urllib3.disable_warnings()

search_endpoint = "https://hjp.znanje.hr/index.php?show=search"
no_results_msg = "Nijedna natuknica u rječničkoj bazi ne zadovoljava zadane uvjete."
url_xpath = "//*[contains(text(), 'Izravna poveznica za pristup natuknici')]/@href"


@dataclass
class Alternative:
    """Represents one of the alternatives for an ambiguous term."""

    description: str
    url: str


@dataclass
class Result:
    name: str


@dataclass
class Ambiguous(Result):
    """Represents an ambiguous term as a list of its alternatives."""

    alternatives: List[Alternative]


@dataclass
class Term(Result):
    """Represents a single, non-ambiguous term."""

    definitions: List[str]
    etymology: str
    phraseology: Optional[str]
    url: str


@dataclass
class Empty(Result):
    """Represents the case of no results."""

    pass


def get_alternatives(tree) -> List[Alternative]:
    """Heuristically extract alternatives from a disambiguation page."""
    alternatives_node = tree.getparent().getparent().getnext()
    alternatives: List[Alternative] = []

    while (alternatives_node.tag == 'div' and
           alternatives_node.cssselect('div p')):
        alt_node = alternatives_node.cssselect('div p')[0]
        text = alt_node.text_content().replace('detaljnije', '').strip()
        url = urljoin(search_endpoint, alt_node.xpath('a/@href')[0])
        alternatives.append(Alternative(text, url))

        alternatives_node = alternatives_node.getnext()

    return alternatives


def search(search_term: str) -> Result:
    """Search for term and return structured results."""
    req = requests.post(search_endpoint,
                        {'word': search_term, 'search': 'Pretraga'}, verify=False)
    return parse_result_page(search_term, req.content)


def fetch(search_term: str, url: str) -> Result:
    """Search for term and return structured results."""
    req = requests.get(url, verify=False)
    return parse_result_page(search_term, req.content)


def parse_phraseology(tree: html.HtmlElement) -> List[str]:
    def expand_and_partition(acc, p):
        """Expands text nodes and partitions elements into phrases."""
        if p.tag == "br":
            acc.append([])
        else:
            # XXX: If I want to parse this into an AST for better styling (e.g. to
            # be able to tell the difference between the phrase and its explanation),
            # this is the place to do it.
            acc[-1].append(p.text_content().strip() + ":" if p.tag == "b" else "")

            if p.tail is not None:
                acc[-1].append(p.tail.strip())

        return acc

    phraseology_div = tree.cssselect("#frazeologija")[0]

    if not phraseology_div.text_content().strip():
        return None

    phrase_elements = phraseology_div.getchildren()[0].getchildren()
    phrases = reduce(expand_and_partition, phrase_elements, [[]])
    phrases = [" ".join(p) for p in phrases]

    return phrases


def parse_result_page(search_term: str,
                      content: bytes) -> Result:
    """Parse contents of a HJP page.

    Returns either:

    - Term, if the page is a definition page of an unambiguous term
    - Ambiguous, if the page is a disambiguation page
    """
    tree = html.fromstring(content)

    ambiguous = tree.xpath(
        "//p[contains(text(), 'Pronađeno je više natuknica')]")
    no_results = tree.xpath(
        "//p[contains(text(), '{}')]".format(no_results_msg))

    if len(ambiguous):
        alternatives = get_alternatives(ambiguous[0])

        return Ambiguous(search_term, alternatives)
    elif len(no_results):
        return Empty(search_term)
    else:
        try:
            definitions = [d.text_content().strip()
                           for d
                           in tree.cssselect('#definicija table')[0].getchildren()]
        except IndexError:
            definitions = [tree.cssselect('#definicija')[0].text_content().strip()]

        etymology = tree.cssselect('#etimologija')[0].text_content().strip()[2:]
        phraseology = parse_phraseology(tree)
        url = str(tree.xpath(url_xpath)[0])

        term = Term(search_term, definitions, etymology, phraseology, url)
        return term


def input_int(prompt: str) -> Optional[int]:
    """Prompt the user for an int."""
    choice = input(prompt)

    try:
        return int(choice)
    except ValueError:
        return None


def menu(items):
    """Construct and display a menu from items."""
    for i, item in enumerate(items):
        print("{:2}. {}".format(i, item))

    print()

    choice = input_int("Choice: ")

    while choice is None:
        print("Invalid choice, please input a number.")
        choice = input_int("Choice: ")

    while choice >= len(items) or choice < 0:
        print("Invalid choice, input out of bounds.")
        choice = input("Choice: ")

    return int(choice)


def build_parser():
    """Build argument parser."""
    parser = argparse.ArgumentParser()
    parser.add_argument('term', help='Term to search for')
    parser.add_argument('-u', '--url',
                        action='store_true',
                        help='Just print a permalink to the requested term')
    return parser


def render_pretty(term: Term) -> str:
    """Pretty print a result (Term instance)."""

    rendered = f"[yellow]{term.name}[/yellow]\n\n"

    for definition in term.definitions:
        rendered += f"  {definition}\n"

    rendered += f"\n[red]Etimologija: [/red]{term.etymology}\n"

    if term.phraseology:
        rendered += "\n[dim green]Frazeologija:[/dim green]\n"
        for phrase in term.phraseology:
            rendered += f"  [white]- {phrase}\n"

    rendered += f"\n[blue]Ref: [link={term.url}]{term.url}[/link][/blue]\n"

    return rendered


def render_plain(term: Term) -> str:
    """Print a result (Term instance) plainly."""

    rendered = f"{term.name}\n\n"

    for definition in term.definitions:
        rendered += f"  {definition}\n"

    rendered += f"\nEtimologija: {term.etymology}\n"

    if term.phraseology:
        rendered += "\nFrazeologija:\n"
        for phrase in term.phraseology:
            rendered += f"  {phrase}\n"

    rendered += f"\nRef: <{term.url}>\n"

    return rendered


def main():
    """Main entry point."""
    parser = build_parser()
    args = parser.parse_args()
    result = search(args.term)

    if isinstance(result, Ambiguous):
        print("{} is ambiguous. Choices:\n".format(result.name))
        choice = menu(['{} <{}>'.format(alternative.description,
                                        alternative.url)
                       for alternative in result.alternatives])
        result = fetch(result.name, result.alternatives[choice].url)

    if isinstance(result, Empty):
        print("No results for term: {}".format(result.name))
    elif isinstance(result, Ambiguous):
        print("Error, this shouldn't happen: Link from disambiguation page "
              "leads to another disambiguation page.")
        sys.exit(1)
    elif isinstance(result, Term):
        if args.url:
            print(result.url)
        else:
            if os.isatty(1):
                from rich.panel import Panel
                from rich import print as rprint
                rprint(Panel.fit(render_pretty(result), width=100))
            else:
                print(render_plain(result))


if __name__ == "__main__":
    main()
