This is a simple command-line client for [Hrvatski Jezični Portal
(HJP)](http://hjp.znanje.hr/). It can be used to retrieve lexicographic
information on Croatian words from the shell.

# Example

```
$ hjp test

hjp.main.Term(
    name='test',
    definitions=[
        '1. a. pokus, ispit ili proba koji treba da dokaže neku pretpostavku '
            'ili tvrdnju [test turnir; test-vožnja; podvrgnuti se testu] b. '
            'standardizirani mjerni postupak ili instrument kojim se mjeri neka '
            'pojava; upotrebljava se u svim disciplinama u kojima se može '
            'egzaktno mjeriti [napraviti test]',
        '2. psih. a. ispitivanje, provjera koja omogućuje da se procijene, '
            'vrednuju psihičke ili fizičke sposobnosti osoba ili da se odrede '
            'značajke njihove ličnosti [test inteligencije; psihotest; sastaviti '
            'test] b. med. pokus kojim se procjenjuju funkcionalni kapaciteti '
            'kakva organa ili sustava organa',
        '3. pokus, provjera koja omogućuje da se stvori predodžba, mišljenje '
            'o nekom ili nečem [test na natječaju za spikere]'
    ],
    etymology='engl. ← lat. testum: zemljani lonac (za topljenje legura) ≃ testari '
        '(testificari): svjedočiti'
)
```
